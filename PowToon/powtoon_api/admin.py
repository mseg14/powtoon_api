from django.contrib import admin

# Register your models here.
from powtoon_api.models import PowToon


# class PowToonAdmin(admin.ModelAdmin):
#     list_display = ('id', 'name', 'user_id')
#
#     def user_id(self, obj):
#         return obj.user.id

@admin.register(PowToon)
class PowToonAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user_id')
    autocomplete_fields = ['shared_with_users']

    def user_id(self, obj):
        return obj.user.id


