from django.conf.urls import url

from powtoon_api import api_controller

urlpatterns = [
    url(r'^get$', api_controller.get),
    url(r'^create$', api_controller.create),
    url(r'^edit$', api_controller.edit),
    url(r'^share$', api_controller.share),
    url(r'^delete$', api_controller.delete),
    url(r'^get-all$', api_controller.get_all),
    url(r'^health-check$', api_controller.health_check),
    url(r'^login$', api_controller.login),
]
