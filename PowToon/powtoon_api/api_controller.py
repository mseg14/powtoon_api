import json

from django.http import JsonResponse
from rest_framework.decorators import api_view, authentication_classes

from powtoon_api.api_authenticator import UsernamePasswordBasicAuth
from powtoon_api.config import GET_POWTOON, CREATE_POWTOON, EDIT_POWTOON, DELETE_POWTOON, SHARE_POWTOON
from powtoon_api.info_updater.api_requests_info_mediator import Mediator
from powtoon_api.validators.request_validator import validate_request


@api_view(['GET'])
def health_check(request):
    return JsonResponse({"status": "SUCCESS"})


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def login(request):
    return JsonResponse({"status": "SUCCESS"})


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def get(request):
    validation_response = validate_request(request, GET_POWTOON)
    if not validation_response.was_successful:
        return JsonResponse(validation_response.message)
    mediator = Mediator()
    response = mediator.mediate_get_request(user=request.user, powtoon_id=request.POST['powtoon_id'])
    return JsonResponse(response.message)


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def create(request):
    validation_response = validate_request(request, CREATE_POWTOON)
    if not validation_response.was_successful:
        return JsonResponse(validation_response.message)
    mediator = Mediator()
    response = mediator.mediate_create_request(user=request.user, name=request.POST['name'],
                                               content=request.POST['content'])
    return JsonResponse(response.message)


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def edit(request):
    validation_response = validate_request(request, EDIT_POWTOON)
    if not validation_response.was_successful:
        return JsonResponse(validation_response.message)
    mediator = Mediator()
    response = mediator.mediate_edit_request(user=request.user, name=request.POST['name'],
                                             content=request.POST['content'], powtoon_id=request.POST['powtoon_id'])
    return JsonResponse(response.message)


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def delete(request):
    validation_response = validate_request(request, DELETE_POWTOON)
    if not validation_response.was_successful:
        return JsonResponse(validation_response.message)
    mediator = Mediator()
    response = mediator.mediate_delete_request(user=request.user, powtoon_id=request.POST['powtoon_id'])
    return JsonResponse(response.message)


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def share(request):
    validation_response = validate_request(request, SHARE_POWTOON)
    if not validation_response.was_successful:
        return JsonResponse(validation_response.message)
    try:
        users_ids = json.loads(request.POST['users_ids'])
    except Exception:
        return JsonResponse({"message": "invalid users_ids, please supply a list of integers"})
    mediator = Mediator()
    response = mediator.mediate_share_request(user=request.user, powtoon_id=request.POST['powtoon_id'],
                                              users_ids=users_ids)
    return JsonResponse(response.message)


@api_view(['POST'])
@authentication_classes([UsernamePasswordBasicAuth])
def get_all(request):
    mediator = Mediator()
    response = mediator.mediate_get_all_request(user=request.user)
    return JsonResponse(response.message)
