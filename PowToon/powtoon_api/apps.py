from django.apps import AppConfig


class PowtoonApiConfig(AppConfig):
    name = 'powtoon_api'
