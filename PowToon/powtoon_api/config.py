GET_POWTOON = 'get'
CREATE_POWTOON = 'create'
EDIT_POWTOON = 'edit'
DELETE_POWTOON = 'delete'
GET_ALL_POWTOON = 'get_all'
SHARE_POWTOON = 'share'

REQUIRED_FIELDS = {
    GET_POWTOON: ["powtoon_id"],
    CREATE_POWTOON: ["name", "content"],
    EDIT_POWTOON: ["powtoon_id", "name", "content"],
    SHARE_POWTOON: ["powtoon_id", "users_ids"],
    DELETE_POWTOON: ["powtoon_id"],
}
