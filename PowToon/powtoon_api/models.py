from django.contrib.auth.models import User
from django.db import models
from django.core import serializers


class PowToon(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    content = models.JSONField(default=dict)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    shared_with_users = models.ManyToManyField(User, related_name="users", blank=True)

    def to_json(self):
        return serializers.serialize('json', [self])
