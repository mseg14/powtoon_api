from django.contrib.auth import authenticate
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication


class UsernamePasswordBasicAuth(BaseAuthentication):
    def authenticate(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user:
            return user, None
        raise exceptions.AuthenticationFailed("Failed to authenticate user, please verify credentials")
