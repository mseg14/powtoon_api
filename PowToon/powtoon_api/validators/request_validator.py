from typing import List

from powtoon_api.config import GET_POWTOON, GET_ALL_POWTOON, EDIT_POWTOON, CREATE_POWTOON, DELETE_POWTOON, \
    SHARE_POWTOON, REQUIRED_FIELDS

SUPPORTED_REQUESTS = [GET_POWTOON, GET_ALL_POWTOON, EDIT_POWTOON, CREATE_POWTOON, DELETE_POWTOON, SHARE_POWTOON]


class ValidationResponse:
    def __init__(self, was_successful: bool, message: dict):
        self.was_successful = was_successful
        self.message = message


def validate_request(request, method_name: str) -> ValidationResponse:
    if method_name not in SUPPORTED_REQUESTS:
        return ValidationResponse(
            was_successful=False,
            message={"message": "The requests method: {} is currently unsupported".format(method_name)}
        )

    missing_fields = get_missing_fields(request, REQUIRED_FIELDS[method_name])
    if missing_fields:
        message = {"status": "FAILED",
                   "message": "Lacked required fields for {} action".format(method_name),
                   "missing_fields": missing_fields}
        return ValidationResponse(was_successful=False, message=message)
    else:
        return ValidationResponse(was_successful=True, message={})


def get_missing_fields(request, required_fields=List[str]):
    missing_fields = []
    for field in required_fields:
        if field not in request.POST:
            missing_fields.append(field)
    return missing_fields
