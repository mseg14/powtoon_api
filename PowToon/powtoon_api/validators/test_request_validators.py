import pytest

from powtoon_api.config import GET_POWTOON, EDIT_POWTOON, CREATE_POWTOON, DELETE_POWTOON, \
    SHARE_POWTOON, REQUIRED_FIELDS
from powtoon_api.validators.request_validator import validate_request, get_missing_fields


class FakeRequest:
    def __init__(self, request_date):
        self.POST = request_date


def test_get_missing_fields_valid_input():
    fake_request = FakeRequest({"powtoon_id": "1"})
    res = get_missing_fields(fake_request, REQUIRED_FIELDS[GET_POWTOON])
    assert len(res) == 0


def test_get_missing_fields_invalid_input():
    fake_request = FakeRequest({"fake": "fake"})
    res = get_missing_fields(fake_request, REQUIRED_FIELDS[GET_POWTOON])
    assert len(res) == 1


def test_validate_get_request_valid():
    fake_request = FakeRequest({"powtoon_id": "1"})
    validation_response = validate_request(fake_request, GET_POWTOON)
    assert validation_response.was_successful


def test_validate_get_request_invalid():
    fake_request = FakeRequest({"fake": "1"})
    validation_response = validate_request(fake_request, GET_POWTOON)
    assert not validation_response.was_successful


def test_validate_create_request_valid():
    fake_request = FakeRequest({"name": "name", "content": "test"})
    validation_response = validate_request(fake_request, CREATE_POWTOON)
    assert validation_response.was_successful


def test_validate_create_request_invalid():
    fake_request = FakeRequest({"name": "name"})
    validation_response = validate_request(fake_request, CREATE_POWTOON)
    assert not validation_response.was_successful
    assert validation_response.message['missing_fields'] == ["content"]


def test_validate_edit_request_valid():
    fake_request = FakeRequest({"powtoon_id": "1", "name": "name", "content": "test"})
    validation_response = validate_request(fake_request, EDIT_POWTOON)
    assert validation_response.was_successful


def test_validate_edit_request_invalid():
    fake_request = FakeRequest({"name": "name", "content": "test"})
    validation_response = validate_request(fake_request, EDIT_POWTOON)
    assert not validation_response.was_successful
    assert validation_response.message['missing_fields'] == ["powtoon_id"]


def test_validate_share_request_valid():
    fake_request = FakeRequest({"powtoon_id": "1", "users_ids": [1, 2, 3]})
    validation_response = validate_request(fake_request, SHARE_POWTOON)
    assert validation_response.was_successful


def test_validate_share_request_invalid():
    fake_request = FakeRequest({"powtoon_id": "1"})
    validation_response = validate_request(fake_request, SHARE_POWTOON)
    assert not validation_response.was_successful
    assert validation_response.message['missing_fields'] == ["users_ids"]


def test_validate_delete_request_valid():
    fake_request = FakeRequest({"powtoon_id": "1"})
    validation_response = validate_request(fake_request, DELETE_POWTOON)
    assert validation_response.was_successful


def test_validate_delete_request_invalid():
    fake_request = FakeRequest({})
    validation_response = validate_request(fake_request, DELETE_POWTOON)
    assert not validation_response.was_successful
    assert validation_response.message['missing_fields'] == ["powtoon_id"]
