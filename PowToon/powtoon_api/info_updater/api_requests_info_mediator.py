from typing import List

from django.contrib.auth.models import User

from powtoon_api.info_updater.mediators.create_request_mediator import MediatorCreateRequest
from powtoon_api.info_updater.mediators.delete_request_mediator import MediatorDeleteRequest
from powtoon_api.info_updater.mediators.edit_request_mediator import MediatorEditRequest
from powtoon_api.info_updater.mediators.get_all_request_mediator import MediatorGetAllRequest
from powtoon_api.info_updater.mediators.get_request_mediator import MediatorGetRequest
from powtoon_api.info_updater.mediators.share_request_mediator import MediatorShareRequest


class Mediator:
    def mediate_get_request(self, user: User, powtoon_id: int):
        return MediatorGetRequest().make_request(user=user, powtoon_id=powtoon_id)

    def mediate_create_request(self, user: User, name: str, content: dict):
        return MediatorCreateRequest().make_request(user=user, name=name, content=content)

    def mediate_edit_request(self, user: User, name: str, content: dict, powtoon_id: int):
        return MediatorEditRequest().make_request(user=user, name=name, content=content, powtoon_id=powtoon_id)

    def mediate_delete_request(self, user: User, powtoon_id: int):
        return MediatorDeleteRequest().make_request(user=user, powtoon_id=powtoon_id)

    def mediate_share_request(self, user: User, powtoon_id: int, users_ids: List[int]):
        return MediatorShareRequest().make_request(user=user, powtoon_id=powtoon_id, users_ids=users_ids)

    def mediate_get_all_request(self, user: User):
        return MediatorGetAllRequest().make_request(user=user)
