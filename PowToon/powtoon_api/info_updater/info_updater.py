import json
from typing import List

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

from powtoon_api.custom_exceptions import LackingPermissionToPowToon
from powtoon_api.models import PowToon

CAN_SHARE_POWTOON_AUTH = 'auth.can_share_powtoon'
CAN_GET_ANY_POWTOON = 'auth.can_get_any_powtoon'


def get(user: User, powtoon_id: int) -> PowToon:
    powtoon = PowToon.objects.get(id=powtoon_id)
    if powtoon.user != user and user not in powtoon.shared_with_users.all() and not user.has_perm(CAN_GET_ANY_POWTOON):
        raise LackingPermissionToPowToon
    return powtoon


def create(user: User, name: str, content: dict) -> PowToon:
    name = validate_name_field(name)
    powtoon = PowToon(user=user, name=name, content=content)
    powtoon.save()
    return powtoon


def share(user: User, powtoon_id: int, users_ids: List[int]) -> List[int]:
    try:
        powtoon = PowToon.objects.get(id=powtoon_id)
    except ObjectDoesNotExist:
        raise ObjectDoesNotExist("Requested PowToon of id: {}, does not exist".format(powtoon_id))
    except ValueError:
        raise ValueError("Invalid PowToon id: {}, please supply digits".format(powtoon_id))
    if not user.has_perm(CAN_SHARE_POWTOON_AUTH) or (powtoon.user != user and user not in powtoon.shared_with_users.all()):
        raise LackingPermissionToPowToon
    shared_with = []
    for new_user_id in users_ids:
        try:
            new_user = User.objects.get(id=new_user_id)
        except ObjectDoesNotExist:
            raise ObjectDoesNotExist("Requested user of id: {}, does not exist".format(new_user_id))
        except ValueError:
            raise ValueError("Invalid user id: {}, please supply digits".format(new_user_id))
        powtoon.shared_with_users.add(new_user)
        shared_with.append(new_user_id)
    return shared_with


def edit(user: User, powtoon_id: int, name: str, content):
    name = validate_name_field(name)
    powtoon = PowToon.objects.get(id=powtoon_id)
    if powtoon.user != user:
        raise LackingPermissionToPowToon
    powtoon.name = name
    if type(content) == str:
        try:
            content = json.loads(content[:-2])
        except Exception:
            raise ValueError
    if type(content) != dict:
        raise ValueError
    powtoon.content = content
    powtoon.save()
    return powtoon


def delete(user: User, powtoon_id: int):
    powtoon = PowToon.objects.get(id=powtoon_id)
    if powtoon.user != user:
        raise LackingPermissionToPowToon
    powtoon.delete()


def get_all(user: User) -> List[PowToon]:
    return PowToon.objects.filter(Q(user=user) | Q(shared_with_users__in=[user]))


def validate_name_field(name: str):
    if type(name) != str or name == '' or name.isspace():
        raise ValueError
    return name
