from django.core.exceptions import ObjectDoesNotExist

from powtoon_api.custom_exceptions import LackingPermissionToPowToon
from powtoon_api.info_updater.info_updater import delete
from powtoon_api.info_updater.mediators.mediator_request import MediatorRequest
from powtoon_api.info_updater.mediators.mediator_response import MediatorResponse
from powtoon_api.models import PowToon


class MediatorDeleteRequest(MediatorRequest):
    def make_request(self, **kwargs):
        try:
            delete(kwargs['user'], kwargs['powtoon_id'])
        except ObjectDoesNotExist:
            return self.handle_does_not_exist(powtoon_id=kwargs['powtoon_id'])
        except ValueError:
            return self.handle_invalid_value(powtoon_id=kwargs['powtoon_id'])
        except LackingPermissionToPowToon:
            return self.handle_lacking_permission()
        return self.handle_success(kwargs['powtoon_id'])

    def handle_success(self, powtoon_id) -> MediatorResponse:
        return MediatorResponse(
            success=True,
            message={"status": "SUCCESS",
                     "message": "Successfully deleted PowToon of id: {}".format(powtoon_id)}
        )

    def handle_does_not_exist(self, powtoon_id) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "The requested PowToon object if id: {}, "
                                                    "does not exist".format(powtoon_id)})

    def handle_invalid_value(self, powtoon_id) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "Expected int or string of a digit, "
                                                    "received value: {} is invalid".format(powtoon_id)
                                         })

    def handle_lacking_permission(self) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "You are not permitted to delete the requested powtoon"})
