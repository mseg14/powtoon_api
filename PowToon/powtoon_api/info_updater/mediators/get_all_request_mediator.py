from typing import List

from powtoon_api.info_updater.info_updater import get_all
from powtoon_api.info_updater.mediators.mediator_request import MediatorRequest
from powtoon_api.info_updater.mediators.mediator_response import MediatorResponse
from powtoon_api.models import PowToon


class MediatorGetAllRequest(MediatorRequest):
    def make_request(self, **kwargs):
        all_powtoons = get_all(kwargs['user'])
        return self.handle_success(all_powtoons)

    def handle_success(self, all_powtoons: List[PowToon]) -> MediatorResponse:
        return MediatorResponse(
            success=True,
            message={"status": "SUCCESS",
                     "message": [powtoon.to_json() for powtoon in all_powtoons]}
        )
