from powtoon_api.info_updater.mediators.mediator_response import MediatorResponse


class MediatorRequest:
    def make_request(self, **kwargs):
        pass

    def handle_success(self, **kwargs) -> MediatorResponse:
        pass

    def handle_does_not_exist(self, **kwargs) -> MediatorResponse:
        pass

    def handle_invalid_value(self, **kwargs) -> MediatorResponse:
        pass

    def handle_lacking_permission(self, **kwargs) -> MediatorResponse:
        pass
