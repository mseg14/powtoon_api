from django.core.exceptions import ObjectDoesNotExist
from django.db import DataError

from powtoon_api.custom_exceptions import LackingPermissionToPowToon
from powtoon_api.info_updater.info_updater import edit
from powtoon_api.info_updater.mediators.mediator_request import MediatorRequest
from powtoon_api.info_updater.mediators.mediator_response import MediatorResponse


class MediatorEditRequest(MediatorRequest):
    def make_request(self, **kwargs):
        try:
            powtoon = edit(user=kwargs['user'], name=kwargs['name'], content=kwargs['content'],
                           powtoon_id=kwargs['powtoon_id'])
        except ObjectDoesNotExist:
            return self.handle_does_not_exist(powtoon_id=kwargs['powtoon_id'])
        except (ValueError, DataError):
            return self.handle_invalid_value(powtoon_id=kwargs['powtoon_id'])
        except LackingPermissionToPowToon:
            return self.handle_lacking_permission()
        return self.handle_success(powtoon=powtoon)

    def handle_success(self, **kwargs) -> MediatorResponse:
        return MediatorResponse(success=True,
                                message={'status': 'SUCCESS', 'message': kwargs['powtoon'].to_json()})

    def handle_does_not_exist(self, **kwargs) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "The requested PowToon object of id: {}, "
                                                    "does not exist".format(kwargs['powtoon_id'])})

    def handle_invalid_value(self, **kwargs) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "Failed to edit powtoon of id: {}."
                                                    "please verify the supplied name is of at least length 1,"
                                                    "and of no more then 100 and that the content field is a valid JSON"
                                         })

    def handle_lacking_permission(self) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "You are not permitted to edit the requested powtoon"})
