from typing import List

from django.core.exceptions import ObjectDoesNotExist
from django.db import DataError

from powtoon_api.custom_exceptions import LackingPermissionToPowToon
from powtoon_api.info_updater.info_updater import share
from powtoon_api.info_updater.mediators.mediator_request import MediatorRequest
from powtoon_api.info_updater.mediators.mediator_response import MediatorResponse


class MediatorShareRequest(MediatorRequest):
    def make_request(self, **kwargs):
        try:
            shared_with = share(kwargs['user'], kwargs['powtoon_id'], kwargs['users_ids'])
        except ObjectDoesNotExist as e:
            return self.handle_does_not_exist(powtoon_id=kwargs['powtoon_id'], exception_message=e)
        except (ValueError, DataError) as e:
            return self.handle_invalid_value(powtoon_id=kwargs['powtoon_id'], exception_message=e)
        except LackingPermissionToPowToon:
            return self.handle_lacking_permission()
        return self.handle_success(shared_with)

    def handle_success(self, shared_with: List[int]) -> MediatorResponse:
        return MediatorResponse(
            success=True,
            message={"status": "SUCCESS",
                     "message": "Succesfully shared PowToon with the following users: {}".format(shared_with)}
        )

    def handle_does_not_exist(self, powtoon_id, exception_message) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": exception_message.args[0]})

    def handle_invalid_value(self, powtoon_id, exception_message) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": exception_message.args[0]})

    def handle_lacking_permission(self) -> MediatorResponse:
        return MediatorResponse(success=False,
                                message={"status": "FAILED",
                                         "message": "You are not permitted to share the requested powtoon"})
