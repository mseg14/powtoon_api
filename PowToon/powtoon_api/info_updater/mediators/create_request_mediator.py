from django.db import DataError

from powtoon_api.info_updater.info_updater import create
from powtoon_api.info_updater.mediators.mediator_request import MediatorRequest
from powtoon_api.info_updater.mediators.mediator_response import MediatorResponse


class MediatorCreateRequest(MediatorRequest):
    def make_request(self, **kwargs):
        try:
            powtoon = create(user=kwargs['user'], name=kwargs['name'], content=kwargs['content'])
        except (ValueError, DataError):
            return self.handle_invalid_value(name=kwargs['name'], content=kwargs['content'])
        return self.handle_success(powtoon=powtoon)

    def handle_success(self, **kwargs) -> MediatorResponse:
        return MediatorResponse(success=True,
                                message={'status': 'SUCCESS', 'message': kwargs['powtoon'].to_json()})

    def handle_invalid_value(self, **kwargs) -> MediatorResponse:
        return MediatorResponse(success=False, message={'status': 'FAILED',
                                                        'message': 'Supplied input was invalid. please supply a name '
                                                                   'of no more then 100 chars, and a valid JSON for '
                                                                   'the content'})
