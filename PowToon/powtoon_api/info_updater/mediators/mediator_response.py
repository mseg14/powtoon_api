class MediatorResponse:
    def __init__(self, success: bool, message: dict):
        self.success = success
        self.message = message
